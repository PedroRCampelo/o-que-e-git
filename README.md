# o-que-e-git
Resposta da questão 4 - CactoWeb

## O que é Git e como ele ajuda no dia dia no desenvolvimento de projetos de software?

Atualmente o GIT é o sistema de versionamento de código mais utilizado do mundo.
O GIT tem o poder de registrar as mudanças do código fonte de um objeto permitindo que os arquivos possam ser alterados de forma simultânea. 
O sistema é essencial para o trabalho em equipe, trazendo mais segurança, flexibilidade e desempenho.